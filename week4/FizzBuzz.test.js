const FizzBuzz = require('./FizzBuzz')

test('check', () => {
    for (i = 1; i < 101; i++) {
        if (i % 3 != 0 && i % 5 != 0) {
            expect(FizzBuzz(i)).toBe(i)
        }
        if (i % 5 == 0 && i % 3 == 0) {
            expect(FizzBuzz(i)).toBe('FizzBuzz')
        }
        if (i % 3 == 0 && i % 5 != 0 ) {
            expect(FizzBuzz(i)).toBe('Fizz')
        }
        if (i % 5 == 0 && i % 3 !=0) {
            expect(FizzBuzz(i)).toBe('Buzz')
        }
    }
})

